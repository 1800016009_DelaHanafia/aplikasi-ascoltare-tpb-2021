<p align="center">
 <img width="100px" src="https://gitlab.com/1800016009_DelaHanafia/aplikasi-ascoltare-tpb-2021/-/design_management/designs/259537/92109a6f1108ffd322461266ddbb93103fa8c3fc/resized_image/v432x230" align="center" alt="GitHub Readme Stats" />
 <h2 align="center"><b>ASCOLTARE</b></h2>
 <p align="center"><b>Music Streaming and Downloading , manjakan telinga anda dengan musik india!</b></p>
</p>

<h3 align="center">Show some :heart: and :star: the Repo</h3>



  <h3 align="center">Features</h3>
  <p align="center">
    Online Song Search :mag:<br>
    Streaming Support :musical_note:<br>
    Offline Download Support :arrow_down:<br>
    320Kbps m4a/mp3 Format :fire:<br>
    ID3 Tags Attached :notes:<br>
  	Lyrics Support :pencil:<br>

---

  <h3 align="center">Screenshots</h3>
  
 
:-------------------------:|:-------------------------:
![](https://telegra.ph/file/5182d60b3483c5abb60d9.png)  |  
---

  <h3 align="center">Download</h3>
  <p align="center" ><a href="https://gitlab.com/1800016009_DelaHanafia/aplikasi-ascoltare-tpb-2021" rel="GitHub Releases"><img width="100" height="100" src=""></a></p>


