import 'package:flutter/material.dart';
import 'package:gradient_widgets/gradient_widgets.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:Musify/helper/utils.dart';
import 'package:Musify/style/appColors.dart';

class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xff384850),
            Color(0xff263238),
            Color(0xff263238),
          ],
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          brightness: Brightness.dark,
          centerTitle: true,
          title: GradientText(
            "Tentang Ascoltare",
            shaderRect: Rect.fromLTWH(13.0, 0.0, 100.0, 50.0),
            gradient: LinearGradient(colors: [
              Color(0xff4db6ac),
              Color(0xff61e88a),
            ]),
            style: TextStyle(
              color: accent,
              fontSize: 25,
              fontWeight: FontWeight.w700,
            ),
          ),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: accent,
            ),
            onPressed: () => Navigator.pop(context, false),
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        body: SingleChildScrollView(child: AboutCards()),
      ),
    );
  }
}

class AboutCards extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        children: <Widget>[
          Padding(
            padding:
                const EdgeInsets.only(top: 20, left: 8, right: 8, bottom: 6),
            child: Column(
              children: <Widget>[
                ListTile(
                  title: Image.network(
                    "https://gitlab.com/1800016009_DelaHanafia/aplikasi-ascoltare-tpb-2021/-/issues/3/designs/ic_launcher.png",
                    height: 120,
                  ),
                  subtitle: Padding(
                    padding: const EdgeInsets.all(13.0),
                    child: Center(
                      child: Text(
                        "ASCOLTARE | 2.0.21 "
                         " BY : MINORITAS",
                        style: TextStyle(
                            color: accentLight,
                            fontSize: 24,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0, left: 10, right: 10),
            child: Divider(
              color: Colors.white24,
              thickness: 0.8,
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(top: 8, left: 8, right: 8, bottom: 6),
            child: Card(
              color: Color(0xff263238),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
              elevation: 2.3,
              child: ListTile(
                leading: Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: NetworkImage("https://web.telegram.org/358a1be1-0d8b-47cb-8f33-41b5ba1a22a4"),
                    ),
                  ),
                ),
                title: Text(
                  'Johan Efendi (1800016018)',
                  style: TextStyle(color: accentLight),
                ),
                subtitle: Text(
                  'App Developer | UX/UI Design',
                  style: TextStyle(color: accentLight),
                ),
                trailing: Wrap(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(
                        MdiIcons.telegram,
                        color: accentLight,
                      ),
                      tooltip: 'Hubungi Lewat  Telegram',
                      onPressed: () {
                        launchURL("https://telegram.dog/johanefendi_19");
                      },
                    ),
                    IconButton(
                      icon: Icon(
                        MdiIcons.instagram,
                        color: accentLight,
                      ),
                      tooltip: 'Hubungi lewat Instagram',
                      onPressed: () {
                        launchURL("https://www.instagram.com/johanefenndi/");
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(top: 8, left: 8, right: 8, bottom: 6),
            child: Card(
              color: Color(0xff263238),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              elevation: 2.3,
              child: ListTile(
                leading: Container(
                  width: 50.0,
                  height: 50,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: NetworkImage(
                          "https://web.telegram.org/a07bc7ca-2804-4c6c-a514-0580bc0433e2"),
                    ),
                  ),
                ),
                title: Text(
                  'Dela Hanafia (1800016009)',
                  style: TextStyle(color: accentLight),
                ),
                subtitle: Text(
                  'App Developer | UI/UX Design',
                  style: TextStyle(color: accentLight),
                ),
                trailing: Wrap(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(
                        MdiIcons.telegram,
                        color: accentLight,
                      ),
                      tooltip: 'hubungi lewat Telegram',
                      onPressed: () {
                        launchURL("https://telegram.dog/delahanafia21");
                      },
                    ),
                    IconButton(
                      icon: Icon(
                        MdiIcons.instagram,
                        color: accentLight,
                      ),
                      tooltip: 'hubungi lewat instagram',
                      onPressed: () {
                        launchURL("https://instagram.com/delahanafia21");
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(top: 8, left: 8, right: 8, bottom: 6),
            child: Card(
              color: Color(0xff263238),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              elevation: 2.3,
              child: ListTile(
                leading: Container(
                  width: 50.0,
                  height: 50,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: NetworkImage(
                          "https://fontawesome.com/v5.15/icons/user-tie?style=solid"),
                    ),
                  ),
                ),
                title: Text(
                  'Ghiffari Daffa Demaal (1711016096)',
                  style: TextStyle(color: accentLight),
                ),
                subtitle: Text(
                  'App Developer | UI/UX Design ',

                  style: TextStyle(color: accentLight),
                ),
                trailing: Wrap(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(
                        MdiIcons.telegram,
                        color: accentLight,
                      ),
                      tooltip: 'hubungi lewat telegram',
                      onPressed: () {
                        launchURL("https:telegram.com");
                      },
                    ),
                    IconButton(
                      icon: Icon(
                        MdiIcons.instagram,
                        color: accentLight,
                      ),
                      tooltip: 'hubungi lewat instagram',
                      onPressed: () {
                        launchURL("https://instagram.com");
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
